n_dim = 64;
n = n_dim * n_dim;

x = eye( n );

options.h= compute_wavelet_filter( 'Haar' );

A = zeros( n, n );
for i = 1 : n
    image = reshape( x( :, i ), [ n_dim, n_dim ] );
    IWT=perform_wavortho_transf( image, 0, -1, options );
    fourier_coef = fftshift( fft2( IWT ) ) / n;
    A( :, i ) = reshape( fourier_coef, [ n, 1 ] );
end 

max_coh = max( abs( A ), [], 2 );
coh_structure = reshape( max_coh, [ n_dim, n_dim ] );

imshow( coh_structure, [0 4/n], 'InitialMagnification', 'fit' );