n_dim = 64;
n = n_dim * n_dim * n_dim;

options.h= compute_wavelet_filter( 'Daubechies', 4 );

A = zeros( n, 1 );
max_coh = A;

for i = 1 : n
    i
    x = zeros( n, 1 );
    x( i ) = 1;
    image = reshape( x, [ n_dim, n_dim, n_dim ] );
    IWT=perform_wavortho_transf( image, 4, -1, options );
    fourier_coef = fftshift( fftn( IWT ) ) / n_dim^3;
    A = reshape( fourier_coef, [ n, 1 ] );
    max_coh = max( [ max_coh, abs( A ) ], [], 2 );
end

coh_structure = reshape( max_coh, [ n_dim, n_dim, n_dim ] );

isosurface( coh_structure, 0.0005 );